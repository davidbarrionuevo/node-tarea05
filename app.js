const { crearArchivo } = require("./tabla");
var colors = require('colors');
const argv = require("process");

console.log('GENERAR TABLA'.underline.green)

const parametroBase="--base="
const param = process.argv;
const existe = param[2].indexOf(parametroBase);
if (existe >= 0) {
  let ini=parametroBase.length
  const nro = param[2].substring(ini);
  if (nro > 0) crearArchivo(nro);
}
